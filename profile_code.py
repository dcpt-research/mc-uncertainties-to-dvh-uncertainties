import argparse
from main import get_dvh_param

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--dose', type=str, nargs=1, required=True,\
				help='Path to RTDOSE file')	
	parser.add_argument('--dose-std', type=str, nargs=1, required=False,\
				help='Path to RTDOSE file with MC uncertainties in percent')	
	parser.add_argument('--struct', type=str, nargs=1, required=True,\
				help='Path to RTSTRUCT file')
	parser.add_argument('--ct', type=str, nargs=1, required=True,\
				help='Path to CT file')
	parser.add_argument('--structure-name', type=str, nargs=1, required=True,\
				help='Name of the structure you want a DVH for')
	parser.add_argument('--robustness', type=int, nargs=1, required=False,\
				help='Robustness evaluation using x-y-z isocenter-shift in mm')
	parser.add_argument('-o','--output', type=str, nargs=1, required=False,\
				help='Path to output png image')
	parser.add_argument('--dvh-param', type=str, nargs=1, required=False,\
				help='Supply a DVH parameter, such as V20Gy. You can only supply volumes in %%.')
	args = parser.parse_args()

	dose_std_file = None
	if args.dose_std:
		dose_std_file = args.dose_std[0]
	outputfile = None
	if args.output:
		outputfile = args.output[0]
	if args.dvh_param:
		import cProfile

		pr = cProfile.Profile()
		pr.enable()

		dvh_parameter, std_paramter = get_dvh_param(args.dose[0], args.struct[0], args.ct[0], args.structure_name[0], args.dvh_param[0], dose_std_file = dose_std_file)

		pr.disable()
		pr.print_stats(sort='cumtime')

		# if args.dvh_param[0][0].lower() == "v":
		# 	print("{} = {} % +- {} %".format(args.dvh_param[0], dvh_parameter, std_paramter))
		# if args.dvh_param[0][0].lower() == "d":
		# 	print("{} = {} Gy +- {} Gy".format(args.dvh_param[0], dvh_parameter, std_paramter))