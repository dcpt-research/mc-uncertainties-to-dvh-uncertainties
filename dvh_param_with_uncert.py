from numpy import max, abs
from scipy.interpolate import interp1d
def dvh_param_with_uncert(dose_volume_histogram, dvh_parameter, dose_volume_histogram_lower_upper = None):
	"""Evaluates the dvh_parameter for the DVH in dose_volume_histogram.
	If dose_volume_histogram_lower_upper is supplied, then an uncertainty band is also supplied for the value.

	Args:
		dose_volume_histogram (numpy.ndarray): Dose volume histogram
		dvh_paramert (str): The DVH parameter you wish to know. Example: V20Gy, V20%, D10%. Only % are supported for DYY parameters.
		dose_volume_histogram_lower_upper (numpy.ndarray, optional): Lower and upper confidence levels of DVH based on statistical uncertainties. Defaults to None.
	"""	
	# First verify that the dvh_parameter follows all the requirements
	if type(dvh_parameter) is not str:
		raise ValueError("The DVH parameter should be a string. Type: {} was supplied".format(type(dvh_parameter)))
	if dvh_parameter[0].lower() not in ["d", "v"]:
		raise ValueError("The DVH parameter should start with either a D or a V (lower or uppercase). {} was supplied".format(dvh_parameter))


	if dvh_parameter[0].lower() == "d":
		return dvh_param_with_uncert_dose(dose_volume_histogram, dvh_parameter, dose_volume_histogram_lower_upper = dose_volume_histogram_lower_upper)
	if dvh_parameter[0].lower() == "v":
		return dvh_param_with_uncert_volume(dose_volume_histogram, dvh_parameter, dose_volume_histogram_lower_upper = dose_volume_histogram_lower_upper)

def dvh_param_with_uncert_dose(dose_volume_histogram, dvh_parameter, dose_volume_histogram_lower_upper = None):
	# Since this is a dose dvh param we know it starts with d and ends with %
	# but of course we verify
	if not dvh_parameter[0].lower() == "d" and dvh_parameter[-1].lower() == "%":
		raise ValueError("The DVH parameter for a dose parameter should start with a D (lower or uppercase) and end with a %. {} was supplied".format(dvh_parameter))
	dvh_param_number = dvh_parameter[1:-1]
	if not dvh_param_number.isnumeric():
		raise ValueError("The DVH parameter for a dose parameter should be a D (lower or uppercase), then an integer and then a %. The middle part is not a number now for: {}".format(dvh_parameter))
	dvh_param_number = int(dvh_param_number)
	if dvh_param_number < 0 or dvh_param_number > 100:
		raise ValueError("The DVH parameter should be between 0 and 100 %. You supplied: {}".format(dvh_parameter))
	
	# Extracting doses and volumes and getting ready for interpolation
	nominal_volumes = dose_volume_histogram[:,1] * 100
	nominal_doses = dose_volume_histogram[:,0]

	nominal_volume_to_dose = interp1d(nominal_volumes, nominal_doses)

	uncert_dvh_param = 0
	if dose_volume_histogram_lower_upper is not None:
		lower_doses = dose_volume_histogram_lower_upper[0][:,0]
		lower_volumes = dose_volume_histogram_lower_upper[0][:,1] * 100
		upper_doses = dose_volume_histogram_lower_upper[1][:,0]
		upper_volumes = dose_volume_histogram_lower_upper[1][:,1] * 100
		upper_volume_to_dose = interp1d(upper_volumes, upper_doses)
		lower_volume_to_dose = interp1d(lower_volumes, lower_doses)
		lower_dvh_param = lower_volume_to_dose(dvh_param_number)
		upper_dvh_param = upper_volume_to_dose(dvh_param_number)
		
		uncert_dvh_param = abs(upper_dvh_param - lower_dvh_param)/2
	
	nominal_dvh_param = nominal_volume_to_dose(dvh_param_number)
	return (nominal_dvh_param, uncert_dvh_param)

	

def dvh_param_with_uncert_volume(dose_volume_histogram, dvh_parameter, dose_volume_histogram_lower_upper = None):
	# End can be "Gy" or "%" so we'll stratify on that
	# Since this is a volume dvh param we know it starts with v and ends with % or gy
	# but of course we verify
	if not dvh_parameter[0].lower() == "v" and (dvh_parameter[-1].lower() == "%" or dvh_parameter[-2:].lower() == "gy"):
		raise ValueError("The DVH parameter for a volume parameter should start with a v (lower or uppercase) and end with a \% or gy. {} was supplied".format(dvh_parameter))
	
	if dvh_parameter[-1].lower() == "%":
		dvh_param_number = dvh_parameter[1:-1]
	if dvh_parameter[-2:].lower() == "gy":
		dvh_param_number = dvh_parameter[1:-2]
	if not dvh_param_number.replace(".","").isnumeric():
		raise ValueError("The DVH parameter for a volume parameter should be a v (lower or uppercase), then a number and then a \% or gy. The middle part is not a number now for: {}".format(dvh_parameter))
	dvh_param_number = float(dvh_param_number)
	if dvh_param_number < 0 or dvh_param_number > 100:
		raise ValueError("The DVH parameter should be between 0 and 100 \% or 100 gy. You supplied: {}".format(dvh_parameter))

	# Extracting doses and volumes and getting ready for interpolation
	nominal_volumes = dose_volume_histogram[:,1] * 100
	nominal_doses = dose_volume_histogram[:,0]

	# In case we are looking for a percentage volume:
	if dvh_parameter[-1].lower() == "%":
		nominal_doses = nominal_doses / max(nominal_doses) * 100

	nominal_dose_to_volume = interp1d(nominal_doses,nominal_volumes)

	uncert_dvh_param = 0
	if dose_volume_histogram_lower_upper is not None:
		lower_doses = dose_volume_histogram_lower_upper[0][:,0]
		lower_volumes = dose_volume_histogram_lower_upper[0][:,1] * 100
		upper_doses = dose_volume_histogram_lower_upper[1][:,0]
		upper_volumes = dose_volume_histogram_lower_upper[1][:,1] * 100

		if dvh_parameter[-1].lower() == "%":
			lower_doses = lower_doses / max(lower_doses) * 100
			upper_doses = upper_doses / max(upper_doses) * 100


		upper_dose_to_volume = interp1d(upper_doses, upper_volumes)
		lower_dose_to_volume = interp1d(lower_doses, lower_volumes)
		lower_dvh_param = lower_dose_to_volume(dvh_param_number)
		upper_dvh_param = upper_dose_to_volume(dvh_param_number)
		
		uncert_dvh_param = abs(upper_dvh_param - lower_dvh_param)/2
	

	nominal_dvh_param = nominal_dose_to_volume(dvh_param_number)
	return (nominal_dvh_param, uncert_dvh_param)
