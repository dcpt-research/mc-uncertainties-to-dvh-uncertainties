import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from math import ceil
from code_interact import code_interact

def plot_and_save_dvh(dose_volume_histogram, output, structure_name, dose_volume_histogram_lower_upper = None):
	if dose_volume_histogram_lower_upper is not None:
		plot_dvh_with_uncert(dose_volume_histogram, dose_volume_histogram_lower_upper, output, structure_name)
		return
	plot_simple_dvh(dose_volume_histogram, output, structure_name)
	return


def plot_dvh_with_uncert(dose_volume_histogram, dose_volume_histogram_lower_upper, output, structure_name):

	fig,ax = plt.subplots(2, 2, gridspec_kw={'height_ratios': [3, 1], 'width_ratios' : [3, 1]})
	fig.suptitle("DVH for {}".format(structure_name))
	ax[0,0].grid()
	ax[0,1].grid()
	ax[1,0].grid()

	dose_nominal = dose_volume_histogram[:,0]
	vol_nominal  = dose_volume_histogram[:,1]

	ax[1,1].axis('off')
	ax[0,0].plot(dose_volume_histogram[:,0], 100 * dose_volume_histogram[:,1])
	if dose_volume_histogram_lower_upper is not None:
		ax[0,0].plot(dose_volume_histogram_lower_upper[1][:,0],100*dose_volume_histogram_lower_upper[1][:,1]) # Upper DVH
		ax[0,0].plot(dose_volume_histogram_lower_upper[0][:,0],100*dose_volume_histogram_lower_upper[0][:,1]) # Lower DVH
	ax[0,0].set_ylabel("Relative volume [%]")


	ax[0,1].sharey(ax[0,0])
	ax[1,0].sharex(ax[0,0])


	dose_lower = dose_volume_histogram_lower_upper[0][:,0]
	vol_lower = dose_volume_histogram_lower_upper[0][:,1]
	vol_to_dose_lower = interp1d(vol_lower, dose_lower)
	lower_doses = vol_to_dose_lower(vol_nominal)
	count_0s_in_lower_volumes = len(vol_lower[np.where(vol_lower==0)])
	
	dose_upper = dose_volume_histogram_lower_upper[1][:,0]
	vol_upper = dose_volume_histogram_lower_upper[1][:,1]
	vol_to_dose_upper = interp1d(vol_upper, dose_upper)
	upper_doses = vol_to_dose_upper(vol_nominal)

	vol_to_dose_nominal = interp1d(vol_nominal, dose_nominal)
	nominal_doses = vol_to_dose_nominal(vol_nominal)


	upper_dose_diffs = (upper_doses - nominal_doses)[:-count_0s_in_lower_volumes]
	upper_dose_diffs[np.where(upper_dose_diffs <= 0)] = 0
	ax[0,1].plot(upper_dose_diffs, 100*vol_nominal[:-count_0s_in_lower_volumes])
	lower_dose_diffs = (lower_doses - nominal_doses)[:-count_0s_in_lower_volumes]
	lower_dose_diffs[np.where(lower_dose_diffs >= 0)] = 0
	ax[0,1].plot(lower_dose_diffs, 100*vol_nominal[:-count_0s_in_lower_volumes])	
	ax[0,1].set_xlabel("Dose uncertainty [Gy]")
	x_lims = ceil(np.max([np.max(d) for d in [upper_dose_diffs, -lower_dose_diffs]]))
	ax[0,1].set_xlim([-x_lims,x_lims])


	volume_uncertainty = 1/2 * (dose_volume_histogram_lower_upper[1][:,1] - dose_volume_histogram_lower_upper[0][:,1])
	ax[1,0].plot(dose_volume_histogram_lower_upper[1][:,0], 100 * volume_uncertainty)
	ax[1,0].set_ylabel("Volume uncertainty [%]")
	ax[1,0].set_xlabel("Dose [Gy]")


	plt.savefig(output)


def plot_simple_dvh(dose_volume_histogram, output, structure_name):
	plt.figure()
	plt.plot(dose_volume_histogram[:,0], 100 * dose_volume_histogram[:,1])
	plt.xlabel("Dose [Gy]")
	plt.ylabel("Relative volume [%]")
	plt.title("DVH for {}".format(structure_name))
	plt.savefig(output)