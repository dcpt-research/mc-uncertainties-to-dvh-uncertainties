import pydicom as pd

def loader(dosefile, structfile, ctfile , dose_std_file = None):
	if not dose_std_file:
		return ((pd.read_file(dosefile), None), pd.read_file(structfile), pd.read_file(ctfile))
	return ((pd.read_file(dosefile), pd.read_file(dose_std_file)), pd.read_file(structfile), pd.read_file(ctfile))