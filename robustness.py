import numpy as np
from matplotlib.pyplot import figure, fill_between, xlabel, ylabel, legend, ylim, show, plot, boxplot, title, savefig
from code_interact import code_interact
import os
from scipy.integrate import quad



def line_is_description(line):
	return not all([s.replace(".","").replace("e","").replace("-","").isdigit() for s in line.split()])
def line_is_dvh_numbers(line):
	return all([s.replace(".","").replace("e","").replace("-","").isdigit() for s in line.split()])

def get_structure_name_from_description(description):
	name = ""
	for line in description:
		if "Structure:" in line:
			name = line.split(":")[1]
			break
	if name[0] == " ":
		name = name[1:]
	if name[-1] == " ":
		name = name[:-1]	
	return name


def get_scenario_name_from_description(description):
	if not any(["Uncertainty" in line for line in description]):
		# Nominal case
		return "Nominal"
	for line in description:
		if "Uncertainty" in line:
			name = line.split(":")[1].split("(")[0].replace(" ","")
			break
	return name

def get_volume_multiplication_factor(description, dvh):
	if "%" in description[-1].split()[-1]:
		return 1/100
	return 1/dvh[0,2]
	
def calculate_eud(doses, differential_volumes, n):
	eud_sum = 0
	for dose, vol in zip(doses, differential_volumes):
		eud_sum = eud_sum + dose**(1/n) * vol
	eud_sum = eud_sum**n
	return eud_sum

def calculate_lkb(eud, d50, m):
	t = (eud - d50) / (m * d50)
	front_factor = 1/np.sqrt(2* np.pi)
	def function_to_integrate(x):
		return np.exp(-1/2*x*x)
	integral = quad(function_to_integrate, -np.Infinity, t)
	ntcp = front_factor * integral[0]
	return ntcp



def simple_read(filename):
	dvh_scenarios_by_structurename = {}
	with open(filename, 'r') as f:
		structure = ""
		scenario = ""
		scenario_description = []
		dvh = []
		currently_reading = "description"
		for line in f:
			if line.replace("\n","").replace(":","") == "":
				continue
			line = line.replace("\n","")
			if line_is_description(line):
				if currently_reading == "dvh": # If we were just reading a dvh, then we should put what we have away
					# We already know structure and scenario

					ready_dvh = np.array(dvh)
					ready_dvh[:,2] = ready_dvh[:,2] * get_volume_multiplication_factor(scenario_description, ready_dvh)

					if structure not in dvh_scenarios_by_structurename.keys(): # We have not seen this structure before, so we make the dict
						dvh_scenarios_by_structurename[structure] = {scenario:ready_dvh}
						# scenario is written. Reset everything
						dvh = [];scenario = "";structure=""; scenario_description=[];

					elif structure in dvh_scenarios_by_structurename.keys():
						dvh_scenarios_by_structurename[structure][scenario] = ready_dvh
						# scenario is written. Reset everything
						dvh = [];scenario = "";structure="";scenario_description=[];


				currently_reading = "description"
				scenario_description.append(line)
			if line_is_dvh_numbers(line):
				if currently_reading == "description": 
					structure = get_structure_name_from_description(scenario_description)
					scenario  = get_scenario_name_from_description(scenario_description)

				currently_reading = "dvh"
				dvh.append([float(s) for s in line.split()])
	return dvh_scenarios_by_structurename



if __name__ == "__main__":

	# Code for testing


	patient_numbers = [1,4,8,9,12,14,16,18]

	dvh_foldernames = ["pt{:02}-differential-dvhs".format(i) for i in patient_numbers]



	# dvhs_by_patient = {"dvhs/pt{:02}_dvh.txt".format(pat_i) : simple_read("dvhs/pt{:02}_dvh.txt".format(pat_i)) for pat_i in patient_numbers}

	scans_by_patient = {}
	for fold in dvh_foldernames:
		patientfold = "dvhs/"+fold
		dvhs_by_scan = {}
		for dvhfile in os.listdir(patientfold):
			if dvhfile[-4:] != ".txt":
				continue
			dvhs_by_scan[dvhfile] = simple_read(patientfold+"/"+dvhfile)

		scans_by_patient[patientfold] = dvhs_by_scan

	# n_values 	 = {"RectalWall" : 0.14, "Rectum" : 0.1}
	# m_values 	 = {"RectalWall" : 0.16, "Rectum" : 0.16}
	# d50_values   = {"RectalWall" : 71.6, "Rectum" : 73.6}

	n_values_all = {"Pedersen et al. PT based LKB" : {"RectalWall" : 0.14, "Rectum" : 0.1},
				"QUANTEC": {"Rectum" : 0.09},
				"Model II": {"Rectum" : 0.068},
				"Model III (Emami)": {"Rectum" : 0.12},
				"Model IV": {"Rectum" : 0.12},
				"Model V": {"Rectum" : 0.23},
				"Model VI": {"Rectum" : 0.08},
				}
	m_values_all = {"Pedersen et al. PT based LKB" : {"RectalWall" : 0.16, "Rectum" : 0.16},
				"QUANTEC": {"Rectum" : 0.13},
				"Model II": {"Rectum" : 0.14},
				"Model III (Emami)": {"Rectum" : 0.15},
				"Model IV": {"Rectum" : 0.14},
				"Model V": {"Rectum" : 0.19},
				"Model VI": {"Rectum" : 0.108},
				}
	d50_values_all = {"Pedersen et al. PT based LKB" : {"RectalWall" : 71.6, "Rectum" : 73.6},
				"QUANTEC": {"Rectum" : 76.9},
				"Model II": {"Rectum" : 81},
				"Model III (Emami)": {"Rectum" : 80},
				"Model IV": {"Rectum" : 68.2},
				"Model V": {"Rectum" : 81.9},
				"Model VI": {"Rectum" : 78.4},
				}


	for this_ntcp_model in list(d50_values_all.keys()):
		# this_ntcp_model = "Pedersen et al. PT based LKB"

		n_values = n_values_all[this_ntcp_model]
		m_values = m_values_all[this_ntcp_model]
		d50_values = d50_values_all[this_ntcp_model]

		ntcp_values_by_patient = {} 
		nominal_ntcp_values_by_patient = {}
		

		for patient, scans in scans_by_patient.items():
			ntcp_values_by_patient[patient] = {}
			for scan, dvhs_by_organ in scans.items():
				for organ, vals in n_values.items():
					
					if organ not in list(dvhs_by_organ.keys()):
						continue

					dvhs_by_scenario = dvhs_by_organ[organ]
					if organ not in list(ntcp_values_by_patient[patient].keys()):
						ntcp_values_by_patient[patient][organ] = []
					for scenario, dvh in dvhs_by_scenario.items():
						binsize = dvh[1,0] - dvh[0,0]
						differential_volumes = 100 * dvh[:,2] * binsize
						doses = dvh[:,1]
						total_organ_volume = sum(differential_volumes)
						fractional_differential_volumes = differential_volumes / total_organ_volume
						n = n_values[organ]
						m = m_values[organ]
						d50 = d50_values[organ]
						eud = calculate_eud(doses, fractional_differential_volumes, n)
						ntcp = calculate_lkb(eud,d50,m)
						ntcp_values_by_patient[patient][organ].append(ntcp)
						if scan[5:7] == "00" and scenario == "Nominal":
							nominal_ntcp_values_by_patient[patient] = ntcp

		# Rectalwall based 
		look_at_this_organ = "RectalWall"
		for look_at_this_organ in list(n_values.keys()):
			figure()
			xpos = 1

			all_ntcps = []
			nominals = []

			for patient, ntcp_values_by_organ in ntcp_values_by_patient.items():
				ntcp_values = ntcp_values_by_organ[look_at_this_organ]

				all_ntcps.append(ntcp_values)
				nominals.append(nominal_ntcp_values_by_patient[patient])


			boxplot(all_ntcps)#, label="NTCP value range over all scans")
			plot([i+1 for i in range(8)],nominals, 'b*',label="Nominal scan & scenario")
			xlabel("Patient number")
			ylabel("NTCP")
			title("{} based NTCP ({}) variation".format(look_at_this_organ, this_ntcp_model))
			# ylabel("NTCP")
			legend()
			ylim([-0.05, 1.05])

			ntcp_differences_from_nominal_to_worst_case = [worst_case - nominal for worst_case,nominal in zip([max(n) for n in all_ntcps], nominals)]

			ntcp_increase_mean = np.mean(ntcp_differences_from_nominal_to_worst_case)
			ntcp_increase_std  = np.std(ntcp_differences_from_nominal_to_worst_case)
			print("The worst case NTCP increase when using a {} based model is {} +- {}".format(look_at_this_organ,ntcp_increase_mean, ntcp_increase_std))
			# savefig("{} based NTCP ({}) variation.png".format(look_at_this_organ, this_ntcp_model).replace(" ","_"))


	# show()

			p75_to_p25 = []
			median_to_nominal = []
			for nom, ntcps in zip(nominals, all_ntcps):
				perc_75 = np.percentile(ntcps, 75)
				perc_25 = np.percentile(ntcps, 25)
				p75_to_p25.append(perc_75-perc_25)
				median_to_nominal.append(np.percentile(ntcps,50) - nom)

			print("For model: {} and organ: {}".format(this_ntcp_model, look_at_this_organ))
			print("Differences in 75th and 25th percentiles is in the range [{}, {}] %".format(100*min(p75_to_p25),100*max(p75_to_p25)))
			print("Differences between median and nominal plan is in range [{}, {}] %".format(100*min(median_to_nominal), 100*max(median_to_nominal)))
			code_interact(globals(), locals())