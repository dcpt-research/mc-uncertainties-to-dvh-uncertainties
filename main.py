import argparse
from loader import loader
from mask import mask_structure
from plot_and_save_dvh import plot_and_save_dvh
from dose_volume_histogram import calculate_dose_volume_histogram
from dvh_param_with_uncert import dvh_param_with_uncert


def get_dvh_param(dosefile, structfile, ctfile, structure_name, dvh_parameter, dose_std_file = None):
	(dose, dose_std), struct, ct = loader(dosefile, structfile, ctfile , dose_std_file = dose_std_file)
	masked_dose_ctv, masked_dose_uncertainty = mask_structure(dose, struct, ct, structure_name, dose_uncertainty_dcm = dose_std)
	dvh, (dvh_lower,dvh_upper) = calculate_dose_volume_histogram(masked_dose_ctv, masked_dose_std_array = masked_dose_uncertainty)
	dvh_lower_and_upper = [dvh_lower, dvh_upper]
	if dvh_lower is None or dvh_upper is None:
		dvh_lower_and_upper = None
	return dvh_param_with_uncert(dvh, dvh_parameter, dose_volume_histogram_lower_upper = dvh_lower_and_upper)

def main(dosefile, structfile, ctfile, structure_name, dose_std_file = None, outputfile = None):
	"""Main function! WOOOOO, lets go :-)
	"""

	(dose, dose_std), struct, ct = loader(dosefile, structfile, ctfile , dose_std_file = dose_std_file)

	structname = structure_name

	masked_dose_ctv, masked_dose_uncertainty = mask_structure(dose, struct, ct, structname, dose_uncertainty_dcm = dose_std)

	dvh, (dvh_lower,dvh_upper) = calculate_dose_volume_histogram(masked_dose_ctv, masked_dose_std_array = masked_dose_uncertainty)#masked_dose_std_ctv)

	if outputfile is not None:
		plot_and_save_dvh(dvh, outputfile, structname, dose_volume_histogram_lower_upper=[dvh_lower, dvh_upper])
	
	dvh_param_with_uncert(dvh, "D1%", dose_volume_histogram_lower_upper = [dvh_lower, dvh_upper])
	dvh_param_with_uncert(dvh, "V1%", dose_volume_histogram_lower_upper = [dvh_lower, dvh_upper])
	dvh_param_with_uncert(dvh, "V10Gy", dose_volume_histogram_lower_upper = [dvh_lower, dvh_upper])


	# return dvh, (dvh_lower,dvh_upper)

	


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--dose', type=str, nargs=1, required=True,\
				help='Path to RTDOSE file')	
	parser.add_argument('--dose-std', type=str, nargs=1, required=False,\
				help='Path to RTDOSE file with MC uncertainties in percent')	
	parser.add_argument('--struct', type=str, nargs=1, required=True,\
				help='Path to RTSTRUCT file')
	parser.add_argument('--ct', type=str, nargs=1, required=True,\
				help='Path to CT file')
	parser.add_argument('--structure-name', type=str, nargs=1, required=True,\
				help='Name of the structure you want a DVH for')
	parser.add_argument('--robustness', type=int, nargs=1, required=False,\
				help='Robustness evaluation using x-y-z isocenter-shift in mm')
	parser.add_argument('-o','--output', type=str, nargs=1, required=False,\
				help='Path to output png image')
	parser.add_argument('--dvh-param', type=str, nargs=1, required=False,\
				help='Supply a DVH parameter, such as V20Gy. You can only supply volumes in %%.')
	args = parser.parse_args()

	dose_std_file = None
	if args.dose_std:
		dose_std_file = args.dose_std[0]
	outputfile = None
	if args.output:
		outputfile = args.output[0]
	if args.dvh_param:
		dvh_parameter, std_paramter = get_dvh_param(args.dose[0], args.struct[0], args.ct[0], args.structure_name[0], args.dvh_param[0], dose_std_file = dose_std_file)
		if args.dvh_param[0][0].lower() == "v":
			print("{} = {} % +- {} %".format(args.dvh_param[0], dvh_parameter, std_paramter))
		if args.dvh_param[0][0].lower() == "d":
			print("{} = {} Gy +- {} Gy".format(args.dvh_param[0], dvh_parameter, std_paramter))
	
	masked_dose = main(args.dose[0], args.struct[0], args.ct[0], args.structure_name[0], dose_std_file=dose_std_file, outputfile=outputfile)

	

