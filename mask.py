import numpy as np
import pydicom as pd
from printStructures import printStructures
import cv2

def find_ROI_number(struct_dcm, structure_name):
	# Find the ROINumber first
	ROI_number = -1
	for structure_set_roi_el in struct_dcm.StructureSetROISequence:
		if structure_set_roi_el.ROIName.lower().replace(" ","") == structure_name.lower().replace(" ",""):
			ROI_number = structure_set_roi_el.ROINumber
			return ROI_number
	if ROI_number == -1:
		printStructures(struct_dcm)
		raise ValueError("Could not find {} in the structureset".format(structure_name))



def mask_structure(dose_dcm, struct_dcm, ct, structure_name, dose_uncertainty_dcm = None):
	"""Masks the dosegrid to only include the parts in the structurename found in struct.
	The mask is created in the CT grid.

	Args:
		dose (pydicom.DataSet): RTDOSE DICOM loaded by pydicom
		struct (pydicom.DataSet): RTSTRUCT DICOM loaded by pydicom
		ct (pydicom.DataSet): CT DICOM loaded by pydicom
		structurename (str): Name of the structure in the RTSTRUCT to mask
	Returns:
		a dosegrid with every voxel not in the organ set to -1
	"""	

	
	# Get the dose grid data as a numpy array
	dose_data = dose_dcm.pixel_array * dose_dcm.DoseGridScaling

	# Find the ROINumber first
	ROI_number = find_ROI_number(struct_dcm, structure_name)
	# Get the contour data for the specified structure name
	contour_data = [s.ContourSequence for s in struct_dcm.ROIContourSequence if s.ReferencedROINumber == ROI_number]
	contour_data = [a.ContourData for a in contour_data[0]]

	# If the structure name is not found, return None
	if not contour_data:
		return None

	# Get the z-coordinates for each contour
	z_coords = [dose_dcm.ImagePositionPatient[2] + dose_dcm.SliceThickness * i for i in range(dose_dcm.NumberOfFrames)]

	# Initialize a mask for the dose grid
	mask = np.zeros_like(dose_data, dtype=bool)

	# Loop through each contour and create a binary mask
	for contour in contour_data:
		# Reshape the contour coordinates into separate x, y, and z arrays
		contour = np.array(contour).reshape(-1, 3)
		x = contour[:, 0]
		y = contour[:, 1]
		z = contour[:, 2]

		# Get the indices in the dose grid that correspond to the contour coordinates
		x_indices = (x - dose_dcm.ImagePositionPatient[0]) / dose_dcm.PixelSpacing[0]
		y_indices = (y - dose_dcm.ImagePositionPatient[1]) / dose_dcm.PixelSpacing[1]
		z_indices = [np.abs(z_coords - zi).argmin() for zi in z]

		# Create a binary mask for the current contour
		contour_mask = np.zeros_like(dose_data, dtype=bool)
		# contour_mask[np.round(z_indices).astype(int), np.round(y_indices).astype(int), np.round(x_indices).astype(int)] = True
		contour_mask[np.round(z_indices).astype(int), np.round(y_indices).astype(int), np.round(x_indices).astype(int)] = True


		# Combine the current contour mask with the overall mask
		mask = np.logical_or(mask, contour_mask)

	# Mask out the specified structure from the dose grid
	integer_contour_mask = mask.astype(np.uint8)
	filled_contour_mask = np.zeros_like(integer_contour_mask)
	# We need to loop through every layer to fill out
	for i,layer in enumerate(integer_contour_mask):
		filled_layer = np.zeros_like(layer)
		if layer.any() == 1:
			contours = cv2.findContours(layer, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
			contours = contours[0] if len(contours) == 2 else contours[1]
			the_contour = contours[0]
			x,y,w,h = cv2.boundingRect(the_contour)
			cv2.drawContours(filled_layer, [the_contour], 0, 1, -1)
		filled_contour_mask[i] = filled_layer
		# Now put the filled layer into the filled full contour_mask

	# Now we need to make the array with 1's outside the structure and 0 inside
	# since this is how masked_array function works

	filled_contour_mask = ( (filled_contour_mask.astype('int8') - 1 ) * (-1) ).astype('uint8')

	masked_dose = np.ma.masked_array(dose_data, filled_contour_mask).data

	masked_dose[ np.where(filled_contour_mask == 1) ] = -1

	masked_dose_uncertainty = None
	if dose_uncertainty_dcm:
		dose_uncertainty_data = dose_uncertainty_dcm.pixel_array * dose_uncertainty_dcm.DoseGridScaling
		masked_dose_uncertainty = np.ma.masked_array(dose_uncertainty_data, filled_contour_mask).data
		masked_dose_uncertainty[ np.where(filled_contour_mask == 1) ] = -1

	return (masked_dose, masked_dose_uncertainty)
