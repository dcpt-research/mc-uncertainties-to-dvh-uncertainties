PYTHON3=python3
default : dvh.png

dvh.png : main.py loader.py tests/testdose.dcm tests/teststruct.dcm tests/testct.dcm
	$(PYTHON3) $< -o $@ --dose $(word 3, $^) --struct $(word 4, $^) --ct $(word 5, $^)
	
test : main.py
	$(PYTHON3) $< --dose tests/testdose.dcm --struct tests/teststruct.dcm --ct tests/testct.dcm --dose-std tests/testuncert.dcm --structure-name "Rectum" --dvh-param "V35Gy"
	$(PYTHON3) $< --dose tests/testdose.dcm --struct tests/teststruct.dcm --ct tests/testct.dcm --dose-std tests/testuncert.dcm --structure-name "Rectum" --dvh-param "D5%"
	$(PYTHON3) $< --dose tests/testdose.dcm --struct tests/teststruct.dcm --ct tests/testct.dcm -o ctv_dvh_uncert.png --dose-std tests/testuncert.dcm --structure-name "CTV"
	$(PYTHON3) $< --dose tests/testdose.dcm --struct tests/teststruct.dcm --ct tests/testct.dcm -o rectal_wall_dvh_uncert.png --dose-std tests/testuncert.dcm --structure-name "Rectal Wall"
	$(PYTHON3) $< --dose tests/testdose.dcm --struct tests/teststruct.dcm --ct tests/testct.dcm -o bladder_dvh_uncert.png --dose-std tests/testuncert.dcm --structure-name "Bladder"
	$(PYTHON3) $< --dose tests/testdose.dcm --struct tests/teststruct.dcm --ct tests/testct.dcm -o rectum_dvh_uncert.png --dose-std tests/testuncert.dcm --structure-name "Rectum"
	$(PYTHON3) $< --dose tests/testdose.dcm --struct tests/teststruct.dcm --ct tests/testct.dcm -o body_dvh_uncert.png --dose-std tests/testuncert.dcm --structure-name "Body"

clean :
	$(RM) dvh.png