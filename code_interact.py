import readline                                              
import rlcompleter
import code
def code_interact(globals, locals):
	vars = globals
	vars.update(locals)
	readline.set_completer(rlcompleter.Completer(vars).complete)
	readline.parse_and_bind("tab: complete")
	code.InteractiveConsole(vars).interact()