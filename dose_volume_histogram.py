from numpy import where, prod, shape, sum, max, arange, array

def calculate_dose_volume_histogram(masked_dose_array, masked_dose_std_array = None):
	masked_indices = where(masked_dose_array != -1)

	d_step = 0.01
	masked_dose_array = masked_dose_array[masked_indices]
	if masked_dose_std_array is not None:
		masked_dose_std_array = masked_dose_std_array[masked_indices]
	if masked_dose_std_array is None:
		return (simple_dvh(masked_dose_array, d_step = d_step), [None, None])
	return uncert_dvh(masked_dose_array, masked_dose_std_array, d_step = d_step)

def time_dvh(arr):
	import time
	t0=time.time()
	simple_dvh(arr)
	t1=time.time()
	print("Calling simple_dvh() with {} voxels took: {} s".format(prod(shape(arr)), t1-t0))

def simple_dvh(masked_dose_array, dmax = None, d_step = 0.1):
	
	#1's will be in the mask, so summing is total number
	total_number_of_voxels_in_struct = sum( (masked_dose_array >= 0).astype(int) ) 
	doses = []
	volumes = []
	if dmax is None:
		dmax = max(masked_dose_array)
	for d in arange(0.0, dmax, d_step):
		number_of_voxels_above_d = sum( (masked_dose_array >= d).astype(int) ) 
		doses.append(d)
		volumes.append(number_of_voxels_above_d / total_number_of_voxels_in_struct)
	return array([doses,volumes]).transpose()

def uncert_dvh(masked_dose_array, masked_dose_std_array, d_step = 0.1):
	dm = max(masked_dose_array)*1.05
	
	nominal_dvh = simple_dvh(masked_dose_array, dmax = dm, d_step=d_step)
	
	if masked_dose_std_array is not None:
		upper_dose_array = masked_dose_array + 1 * masked_dose_std_array/100 * masked_dose_array
		lower_dose_array = masked_dose_array - 1 * masked_dose_std_array/100 * masked_dose_array

		lower_dvh = simple_dvh(lower_dose_array, dmax = dm, d_step=d_step)
		upper_dvh = simple_dvh(upper_dose_array, dmax = dm, d_step=d_step)

		return (nominal_dvh, array([lower_dvh, upper_dvh]))
	return (nominal_dvh, [None, None])